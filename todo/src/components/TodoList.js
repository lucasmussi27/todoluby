import React, { useContext } from 'react';
import TodoItem from './TodoItem'
import { TodoContext } from '../contexts/TodoContext';
import { ListBox, List, Empty } from './Styles'

const TodoList = () => {
    const { todos } = useContext(TodoContext)

    return todos.length ? (
        <ListBox>
            <List>
                {todos.map(todo => {
                    return <TodoItem todo={todo} key={todo.id} />
                })}
            </List>
        </ListBox>
        ) : (
        <ListBox>
            <Empty>There's no task to do!</Empty>
        </ListBox>
    )
}

export default TodoList
