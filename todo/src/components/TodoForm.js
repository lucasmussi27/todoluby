import React, { useContext, useState } from 'react';
import { TodoContext } from '../contexts/TodoContext';
import { Form, FormButton, FormInput, FormLabel } from './Styles'

const TodoForm = () => {
    const { dispatch } = useContext(TodoContext)
    const[task, setTask] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch({ type: 'ADD_TODO', todo: {
            task
        }})
        setTask('')
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormLabel className="todo-label" htmlFor="todo">Insert a task to do here:</FormLabel>
            <FormInput className="todo-input" 
                required
                minLength="5"
                type="text" 
                value={task}
                onChange={(e) => setTask(e.target.value)} />
            <FormButton type="submit" value="ADD TASK TO DO" />
        </Form>
    );
}

export default TodoForm
