import React from 'react';
import { ListItem } from './Styles'

export default function TodoItem({ todo }) {
  return (
    <ListItem>
        <p>{todo.task}</p>
    </ListItem>
  );
}
