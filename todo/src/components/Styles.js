import style from 'styled-components'

const AppBox = style.div`
  background: #444;
  margin: 20px auto;
  width: 90%;
  max-width: 700px;
  color: #eee;
  border-radius: 5px;
  padding: 10px;
`

const ListBox = style.div`
  margin: 20px;
`

const List = style.ul`
  padding: 0;
  list-style-type: none;
`

const Empty = style.div`
  margin: 20px;
  text-align: center;
`

const ListItem = style.li`
  background: #333;
  border-radius: 4px;
  padding: 10px;
  margin: 10px 0;
  cursor: pointer;
  text-align: center;
  font-size: 20px;
`

const Form = style.form`
  padding: 20px;
`

const FormInput = style.input`
  width: 100%;
  padding: 10px;
  box-sizing: border-box;
  margin: 10px 0;
  background: #222;
  color: #fff;
  border: 0;
  border-radius: 4px;
`

const FormLabel = style.label`
  font-weight: bold;
  font-size: 18px;
  margin: 10px 0;
`

const FormButton = style.input`
  margin: 10px auto;
  background: #333;
  color: #fff;
  border: 0;
  border-radius: 4px;
  padding: 10px 40px;
  display: block;
  cursor: pointer;
`

export {
  AppBox,
  Empty,
  List,
  ListBox,
  ListItem,
  Form,
  FormButton,
  FormInput,
  FormLabel
}