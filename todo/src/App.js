import React from 'react';
import TodoList from './components/TodoList';
import TodoContextProvider from './contexts/TodoContext';
import TodoForm from './components/TodoForm';
import { AppBox } from './components/Styles';

export default function App() {
  return (
    <AppBox>
      <TodoContextProvider>
        <TodoForm />
        <TodoList />
      </TodoContextProvider>
    </AppBox>
  );
}
